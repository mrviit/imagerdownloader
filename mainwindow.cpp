#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QPicture>
#include <QMessageBox>
#include <QFile>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , nam(new QNetworkAccessManager(this))
    , reply(nullptr)
{
    ui->setupUi(this);

    connect(nam, &QNetworkAccessManager::finished, this, &MainWindow::downloadFinished);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete nam;
    delete reply;
}

void MainWindow::on_pushButton_clicked()
{
    QUrl url(ui->lineEdit->text());
    QNetworkRequest request(url);
    reply = nam->get(request);
    connect(reply, &QNetworkReply::downloadProgress, this, &MainWindow::downloadProgress);
}

void MainWindow::downloadFinished(QNetworkReply *reply)
{
    if(reply->error()) {
        QMessageBox::warning(this, "Error", reply->errorString());
        ui->label->clear();
        ui->progressBar->setValue(0);
        return;
    }

    downloadedData = reply->readAll();
    reply->deleteLater();
    reply = nullptr;

    QMessageBox::information(this, "Successfull", "Downloaded");

    QPixmap pm;
    pm.loadFromData(downloadedData);
    pm.save("igm.png");

    ui->label->setPixmap(pm);
}

void MainWindow::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    ui->progressBar->setMaximum(bytesTotal);
    ui->progressBar->setValue(bytesReceived);
}

void MainWindow::on_pushButton_2_clicked()
{

}

void MainWindow::on_pushButton_3_clicked()
{
    ui->label->clear();
    ui->progressBar->setValue(0);
    //    this->close();
}
